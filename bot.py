import os
import discord
from discord.ext import commands
import youtube_dl
import requests
import feedparser

bot = commands.Bot(command_prefix="/")

# Dictionary to store the RSS feeds
feeds = {}


@bot.event
async def on_ready():
    print(f"Logged in as {bot.user.name}")


@bot.command()
async def search(ctx, search_type, query):
    api_key = os.environ["LISTENNOTES_API_KEY"]
    api_url = "https://listen-api.listennotes.com/api/v2/search"
    api_quota = 300

    # Set the search type based on the parameter value
    search_type = "episode" if search_type == "episode" else "podcast"

    # Make the API request
    headers = {"X-ListenAPI-Key": api_key}
    params = {"q": query, "type": search_type, "only_in": "title,description,author"}
    response = requests.get(api_url, headers=headers, params=params)
    data = response.json()

    # Check if API response indicates reaching the monthly limit
    free_quota = int(response.headers.get("X-ListenAPI-FreeQuota", api_quota))
    usage = int(response.headers.get("X-ListenAPI-Usage", 0))
    if free_quota == 0 and usage >= api_quota:
        await ctx.send(
            "Monthly API request limit reached. Please try again next month."
        )
        return

    # Extract the episode URL or podcast URL from the API response based on the search type
    if search_type == "episode":
        if "results" in data and data["results"]:
            episode_url = data["results"][0]["audio"]
            await play(ctx, episode_url)
        else:
            await ctx.send("No matching episode found.")
    elif search_type == "podcast":
        if "results" in data and data["results"]:
            podcast_url = data["results"][0]["listennotes_url"]
            await ctx.send(f"Podcast found: {podcast_url}")
        else:
            await ctx.send("No matching podcast found.")


@bot.command()
async def play(ctx, url):
    channel = ctx.message.author.voice.channel
    voice_client = await channel.connect()

    if "youtube.com" in url:
        ydl_opts = {
            "format": "bestaudio/best",
            "postprocessors": [
                {
                    "key": "FFmpegExtractAudio",
                    "preferredcodec": "mp3",
                    "preferredquality": "192",
                }
            ],
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(url, download=False)
            url2 = info["formats"][0]["url"]
            voice_client.play(discord.FFmpegPCMAudio(url2))
    elif "podbean.com" in url:
        ydl_opts = {
            "format": "bestaudio/best",
            "postprocessors": [
                {
                    "key": "FFmpegExtractAudio",
                    "preferredcodec": "mp3",
                    "preferredquality": "192",
                }
            ],
            "extractor_args": {
                "source_address": "0.0.0.0",
            },
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(url, download=False)
            url2 = info["formats"][0]["url"]
            voice_client.play(discord.FFmpegPCMAudio(url2))
    else:
        await ctx.send("Unsupported URL")


@bot.command()
async def addfeed(ctx, feed_url):
    # Parse the RSS feed
    feed = feedparser.parse(feed_url)

    # Store the episodes in the feeds dictionary
    if feed.entries:
        feeds[feed_url] = feed.entries
        await ctx.send(f"RSS feed '{feed.feed.title}' added successfully.")
    else:
        await ctx.send("No episodes found in the provided RSS feed.")


@bot.command()
async def listepisodes(ctx, feed_url):
    if feed_url in feeds:
        episodes = feeds[feed_url]
        if episodes:
            episode_list = "\n".join(
                [f"{i+1}. {ep.title}" for i, ep in enumerate(episodes)]
            )
            await ctx.send(f"Episodes in the RSS feed '{feed_url}':\n{episode_list}")
        else:
            await ctx.send("No episodes found in the RSS feed.")
    else:
        await ctx.send("The provided RSS feed is not added.")


@bot.command()
async def playepisode(ctx, feed_url, episode_number):
    if feed_url in feeds:
        episodes = feeds[feed_url]
        if episodes:
            try:
                index = int(episode_number) - 1
                episode = episodes[index]
                episode_url = episode.enclosures[0].href
                await play(ctx, episode_url)
            except IndexError:
                await ctx.send("Invalid episode number.")
            except ValueError:
                await ctx.send("Episode number must be an integer.")
        else:
            await ctx.send("No episodes found in the RSS feed.")
    else:
        await ctx.send("The provided RSS feed is not added.")


@bot.command()
async def pause(ctx):
    voice_client = ctx.voice_client
    if voice_client.is_playing():
        voice_client.pause()
        await ctx.send("Audio paused.")


@bot.command()
async def resume(ctx):
    voice_client = ctx.voice_client
    if voice_client.is_paused():
        voice_client.resume()
        await ctx.send("Audio resumed.")


@bot.command()
async def leave(ctx):
    voice_client = ctx.voice_client
    if voice_client:
        await voice_client.disconnect()
        await ctx.send("Left the voice channel.")


bot.run(os.environ["DISCORD_API_TOKEN"])
