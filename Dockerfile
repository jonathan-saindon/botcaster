# Use the official Python base image
FROM python:3.11-slim-buster

# Set the working directory in the container
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt .

# Install the required dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the bot code into the container
COPY bot.py .

# Run the bot with the passed environment variable
CMD [ "python", "bot.py" ]
